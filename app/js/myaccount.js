$(document).ready(function () {



    $('.js_btn_dark_mode').click(function(){
        $(this).toggleClass('active')
        $('body').toggleClass('--dark')
    })



    $('.js_biling_tab a').click(function(e){
        e.preventDefault();
        if (!$(this).hasClass('active')){
            $('.js_biling_tab a.active').removeClass('active');
            $(this).addClass('active')
            let index = $(this).index();

            $('.js_biling_content .biling_content__item.active').fadeOut(400,function(){
                $(this).removeClass('active')
                $('.js_biling_content .biling_content__item').eq(index).fadeIn(400,function(){
                    $(this).addClass('active')

                })
            })
        }
    })

    $('.js_biling_pay__new input[type="radio"]').change(function(){
        console.log('xs')
        if ($(this).prop('checked')){
            $('.js_biling_pay_method_new_card').slideDown(400)
            $('.js_biling_pay_btn').slideUp(400)
        }
        else{
            $('.js_biling_pay_method_new_card').slideUp(400)
            $('.js_biling_pay_btn').slideDown(400)
        }
    })

    $('.js_biling_pay_method_content__pay_x_item--added input[type="radio"]').change(function(){
        if ($(this).prop('checked')){
            $('.js_biling_pay_method_new_card').slideUp(400)
            $('.js_biling_pay_btn').slideDown(400)
        }
        else{
            $('.js_biling_pay_method_new_card').slideDown(400)
            $('.js_biling_pay_btn').slideUp(400)
        }
    })




    $('.js_biling_content__pay_method_tab a').click(function(e){
        e.preventDefault();
        if (!$(this).hasClass('active')){
            $('.js_biling_content__pay_method_tab a.active').removeClass('active');
            $(this).addClass('active')
            let index = $(this).index();

            $('.js_biling_pay_method_content .biling_pay_method_content__item.active').fadeOut(400,function(){
                $(this).removeClass('active')
                $('.js_biling_pay_method_content .biling_pay_method_content__item').eq(index).fadeIn(400,function(){
                    $(this).addClass('active')

                })
            })
        }
    })







    $('.js_page__account__x__tab a').click(function(e){
        e.preventDefault();
        if (!$(this).hasClass('active')){
            $('.js_page__account__x__tab a.active').removeClass('active');
            $(this).addClass('active')
            let index = $(this).index();

            $('.js_page__account__x__tab__content .page__account__x__tab__content__block.active').fadeOut(400,function(){
                $(this).removeClass('active')
                $('.js_page__account__x__tab__content .page__account__x__tab__content__block').eq(index).fadeIn(400,function(){
                    $(this).addClass('active')

                })
            })
        }
    })


    $('.js_account_x__user_edit__image input').change(function(){
        let $img =  $(this).closest('.js_account_x__user_edit__image').find('img')
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $img.attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    })

    $('.js_btn_menu__myaccount').click(function (e) {
        $('.js_menu__myaccount').toggleClass('active');
        $('.js_btn_menu__myaccount').toggleClass('active');
    })



});