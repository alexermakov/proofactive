$(function () {

    $(window).on("load", function () {
        $('body').addClass('loaded')
    })

    Fancybox.bind('.js__modal', {
        autoFocus: false,
        trapFocus: false,
        closeButton: 'inside',
    });


    $(".js_select").each(function () {
        let placeholder = $(this).attr('placeholder');
        $(this).select2({
            minimumResultsForSearch: 1 / 0,
            placeholder: placeholder,
            // allowClear: true
        })
    });


    $('.js_page__help__faq__x .page__help__faq__x__top').click(function(){
        let parent = $(this).closest('.js_page__help__faq__x');
        parent.find('.page__help__faq__x__content').slideToggle(400,function(){
            parent.toggleClass('active')
        });
    })

    $


    $('.js_home_promo__slider').slick({
        prevArrow:$('.js_home_promo__slider__block_x__arrow button').first(),
        nextArrow:$('.js_home_promo__slider__block_x__arrow button').last()
    })



    $('.js_page__help__faq__block__tabs a').click(function(e){
        e.preventDefault();
        if (!$(this).hasClass('active')){
            $('.js_page__help__faq__block__tabs a.active').removeClass('active');
            $(this).addClass('active')
            let index = $(this).index();

            $('.js_page__help__faq__block__content .page__help__faq__block__content__item.active').fadeOut(400,function(){
                $(this).removeClass('active')
                $('.js_page__help__faq__block__content .page__help__faq__block__content__item').eq(index).fadeIn(400,function(){
                    $(this).addClass('active')

                })
            })
        }
    })

    $('.js_home_control_panel__list .home_control_panel__item').click(function(){
        if (!$(this).hasClass('active')){
            $('.js_home_control_panel__list .home_control_panel__item').removeClass('active')
            $(this).addClass('active')
        }
    })




    $(".js_form__field--password .form__field__eye").click(function () {
        $(this).toggleClass("visible"),
            "password" == $(this).closest(".js_form__field--password").find("input").attr("type") ? $(this).closest(".js_form__field--password").find("input").attr("type", "text") : $(this).closest(".js_form__field--password").find("input").attr("type", "password")
    })




    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });


    $('.js__btn_open__modal').click(function (e) {
        Fancybox.show([{
            src: "#js_modal__call"
        }])
    });


    $('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu_overlay').click(function (e) {
        e.preventDefault();
        $('.js_btn_menu').toggleClass('active')
        $('.js__modal__menu').toggleClass('active')
        $('.js_modal__menu_overlay').toggleClass('active')
    });



    // $('.js_form').submit(function (event) {
    //     if ($(this)[0].checkValidity()) {
    //         let formData = $(this).serialize();
    //         // let formDataAdmin = $(this).serializeArray();
    //         $.ajax({
    //             type: "POST",
    //             url: "obr.php",
    //             data: formData,
    //             success: function (msg) {
    //                 // write_data_admin(formDataAdmin);
    //                   Fancybox.show([{src: "#js_modal_success"}])
    //             }
    //         });
    //     }
    // });

});