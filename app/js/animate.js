$(function () {


    $(".js__animate__left_1").each(function () {
        el = $(this)[0];
        delayTime = 0.15;
        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: -500,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__right_1").each(function () {
        el = $(this)[0];
        delayTime = 0.3;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: 500,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__left").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: -100,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })

    $(".js__animate__right").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: 100,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })

    $(".js__animate__top").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 120%'},
            y: 100,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__bottom").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 120%'},
            y: -150,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__opacity").each(function () {
        let el = $(this)[0]
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })




    $(".js__animate__top_0").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            y: 30,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__scale").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0.;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:$(el).closest('.modern__marketing')[0],start: 'top 50%'},
            scale: 0,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__top_dot").each(function () {
        let el = $(this)[0];
        let delayTime = $(this).data('delay');

        gsap.from(el, 1, {
            scrollTrigger:{trigger:$(el).closest('.modern__marketing__text__list')[0],start: 'top 80%'},
            y: -50,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })



    $(".js__animate__top_line").each(function () {
        let el = $(this)[0]
        let delayTime = $(this).data('delay');

        gsap.from(el, 1, {
            scrollTrigger:{trigger:$(el).closest('.modern__marketing__text__list')[0],start: 'top 80%'},
            x: -100,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


});